Simplest way to use:

```javascript
    document.body.appendChild(savedata.get_dom('Key', 'name.base64'))
```

That's all.

the Key is the key used to store the save data in the localStorage.
the Name.base64 is the name of the save file used in the download save feature.

Additional features:

```javascript
    // The function to be called with the imported data
    import_callback = window.location.reload;
    // The function to be call when export is trigger before retrieving the data
    export_call = game.save;
    document.body.appendChild(savedata.get_dom('Key', 'name.base64', import_callback, export_call))
```
