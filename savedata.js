var savedata = savedata || {};
savedata.debug = false;

savedata.import_base64 = function(key, data, callback) {
	savedata.debug && console.log('[savedata] reading base64');

	savedata.import(key, window.atob(data), callback);
}

savedata.import = function(key, data, callback) {
	savedata.debug && console.log('[savedata] import');
	window.localStorage.setItem(key, data);

	if ( callback ) {
		callback(data);
	}
}

savedata.export = function(key) {
	savedata.save_debug && console.log('[savedata].load');

	return window.localStorage.getItem(key);
}

savedata.export_base64 = function(key) {
	return window.btoa(savedata.export(key));
}

savedata.get_dom = function(key, save_name, import_callback, export_call){
	dom = document.createElement('div');

	dialog = document.createElement('dialog');
	dialog_div = document.createElement('div');
	form = document.createElement('form');
	form.method = 'dialog';

	text_section = document.createElement('section');
	textarea = document.createElement('textarea');
	textarea.rows = 10;
	textarea.cols = 75;
	text_section.appendChild(textarea);

	menu = document.createElement('menu');
	menu.style.float = 'right';

	dialog_close = document.createElement('button');
	dialog_close.textContent = 'Close';
	dialog_close.onclick = function() {
		// TODO: should we actually clear the data?
		textarea.value = '';
		dialog.close()
	}

	dialog_import = document.createElement('button');
	dialog_import.textContent = 'Import';
	dialog_import.onclick = function() {
		// TODO: should we call window.location.reload after importing if no callback is defined?
		savedata.import_base64(key, textarea.value, import_callback || window.location.reload);
		textarea.value = '';
	}

	menu.appendChild(dialog_close);
	menu.appendChild(dialog_import);

	form.appendChild(menu);
	form.appendChild(text_section);

	dialog_div.appendChild(form);
	dialog.appendChild(dialog_div);

	export_button = document.createElement('button');
	export_button.textContent = 'Export';
	export_button.onclick = function() {
		// If a export_call is passed, trigger it
		// Example use case: calling a save function to update the stored save
		if (export_call) {
			export_call();
		}
		
		dialog_import.style.display = 'none';
		textarea.value = savedata.export_base64(key);
		textarea.select();
		dialog.showModal();
	}

	import_button = document.createElement('button');
	import_button.textContent = 'Import';
	import_button.onclick = function() {
		dialog_import.style.display = null;
		textarea.value = "";
		dialog.showModal();
	}

	download_button = document.createElement('button');
	download_button.textContent = 'Download';
	download_button.onclick = function() {
		// If a export_call is passed, trigger it
		// Example use case: calling a save function to update the stored save
		if (export_call) {
			export_call();
		}

		var saveAsBlob = new Blob([ savedata.export_base64(key) ], { type: 'text/plain' });
		var downloadLink = document.createElement("a");

		downloadLink.download = save_name || "save.base64";
		downloadLink.textContent = "Download File";
		downloadLink.href = URL.createObjectURL(saveAsBlob);
		downloadLink.onclick = (event) => {
			// clean up blob after the browser get it
			setTimeout(URL.revokeObjectURL, 100, event.target.href);
			document.body.removeChild(event.target)
		};
		downloadLink.style.display = "none";
		document.body.appendChild(downloadLink);

		downloadLink.click();
	}

	dom.appendChild(export_button);
	// Download before import so that it easier to use
	dom.appendChild(download_button);
	dom.appendChild(import_button);
	dom.appendChild(dialog);

	return dom;
}
